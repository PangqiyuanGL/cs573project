import pandas as pd
import numpy as np
import copy
import sys

# Models
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor, AdaBoostRegressor, BaggingRegressor
from sklearn.kernel_ridge import KernelRidge
from sklearn.linear_model import Ridge, RidgeCV
from sklearn.linear_model import ElasticNet, ElasticNetCV
from sklearn.svm import SVR
#from mlxtend.regressor import StackingCVRegressor
import lightgbm as lgb
from lightgbm import LGBMRegressor
from xgboost import XGBRegressor

class Node:
    def __init__(self, trainset, att, label):
        self.att = att
        self.set = trainset
        self.children = None
        self.label = label 

class DecisionTree(Node):
    def __init__(self, trainset, label, max_depth, min_exp, downsample=False, sample_weights=None):
        self.trainset = trainset
        self.max_depth = max_depth
        self.min_exp = min_exp
        self.label = label
        self.downsample = downsample
        self.set_sample_weights(sample_weights)
        atts = list(self.trainset.keys())
        atts.remove(self.label)
        atts.remove('Id')
        atts.remove('SalePrice')
        atts.remove('sample_weights')
        self.atts = atts
        
    def set_sample_weights(self, sample_weights):
        self.sample_weights = []
        if sample_weights is None:
            self.sample_weights = np.ones(len(self.trainset))
        else:
            self.sample_weights = sample_weights
        self.trainset['sample_weights'] = self.sample_weights
        
    def numofdiffvals(self, series):
        lst = list(series)
        lst.sort()
        pre = lst[0]
        vals = [[pre, 1]]
        count = 1
        num = 1
        for i in range(1, len(lst)):
            if lst[i] != pre:
                count += 1
                pre = lst[i]
                vals.append([pre, 1])
            else:
                vals[-1][1] += 1
        assert count == len(vals)
        assert len(lst) == np.sum([ vals[i][1] for i in range(count)])
        return count, vals  
        
    def probability(self, sets, att):
        vals = dict()
        for i in list(sets.index):
            if sets.loc[i, att] not in list(vals.keys()):
                vals[sets.loc[i, att]] = 0.0
        for i in list(sets.index):
            vals[sets.loc[i, att]] += sets.loc[i, 'sample_weights']
        weights = list(sets.loc[:, 'sample_weights'])
        total = np.sum(weights)
        for key in list(vals.keys()):
            vals[key] /= total
        return vals
        
    def gini(self, sets, att):
        vals = self.probability(sets, att)
        sums = 0.0
        #print(count, vals)
        for i in list(vals.keys()):
            sums += np.square(vals[i])
        return 1 - sums
    
    def indices(self, sets, att, vals):
        ind = dict()
        for i in range(len(vals)):
            ind[vals[i][0]] = []
        for i in list(sets.index):
            for j in range(len(vals)):
                if sets.loc[i,att] == vals[j][0]:
                    ind[vals[j][0]].append(i)
                    break
        return ind
    
    def ginigain(self, sets, att):
        g = self.gini(sets, self.label)
        #print('gini', g, att)
        count, vals = self.numofdiffvals(sets.loc[:, att])
        ind = self.indices(sets, att, vals)
        sums = 0.0
        probs = self.probability(sets, att)
        for i in list(probs.keys()):
            #print(vals[i][1])
            sums += (probs[i])*self.gini(sets.loc[ind[i], :], self.label)
        return g - sums
        
    def maxlabel(self, vals):
        labels = []
        for i in range(len(vals)):
            labels.append(vals[i][1])
        ind = np.argmax(labels)
        label = vals[ind][0]
        #print(vals)
        return label
    
    def recursive(self, sets, atts, depth):
        if depth == self.max_depth or len(sets) <= self.min_exp:
            count, vals = self.numofdiffvals(sets.loc[:, self.label])
            label = self.maxlabel(vals)
            #print('leaf label', label)
            child = Node(None, None, label)
        else:
            if self.downsample:
                maxgg = -1000.0
                maxatt = None
                #print(atts)
                atts1 = list(set(self.atts).difference(set(atts)))
                ind = np.random.choice(len(atts1), int(np.sqrt(len(atts1))), replace=False)
                atts_rf = [atts1[ind[i]] for i in range(len(ind))]
                #atts_rf = list(set(atts_rf).difference(set(atts)))
                for att in atts_rf:
                    gg = self.ginigain(sets, att)
                    #print('gini gain', gg)
                    if gg > maxgg:
                        maxgg = gg
                        maxatt = att
                #print(maxgg, maxatt)
                count, labels = self.numofdiffvals(sets.loc[:, self.label])
                label = self.maxlabel(labels)
                #print(label)
                child = Node(sets, maxatt, label)
                count, vals = self.numofdiffvals(sets.loc[:, maxatt])
                ind = self.indices(sets, maxatt, vals)
                child.children = dict()
                newatts = copy.deepcopy(atts)
                newatts.append(maxatt)
                for i in range(len(ind)):
                    child.children[vals[i][0]] = self.recursive(sets.loc[ind[vals[i][0]], :], newatts, depth+1)
            else:
                maxgg = -10.0
                maxatt = None
                #print(atts)
                for att in atts:
                    gg = self.ginigain(sets, att)
                    #print('gini gain', gg)
                    if gg > maxgg:
                        maxgg = gg
                        maxatt = att
                #print(maxgg, maxatt)
                count, labels = self.numofdiffvals(sets.loc[:, self.label])
                label = self.maxlabel(labels)
                #print(label)
                child = Node(sets, maxatt, label)
                #print(maxatt)
                count, vals = self.numofdiffvals(sets.loc[:, maxatt])
                ind = self.indices(sets, maxatt, vals)
                assert count == len(ind)
                #print(count, maxatt)
                child.children = dict()
                for i in range(len(ind)):
                    #print(maxatt, atts)
                    newatts = copy.deepcopy(atts)
                    newatts.remove(maxatt)
                    #print(vals[i])
                    child.children[vals[i][0]] = self.recursive(sets.loc[ind[vals[i][0]], :], newatts, depth+1)
        return child
             
            
    def fit(self):
        if self.downsample:
            self.decisiontree = self.recursive(self.trainset, [], 0)
        else:
            self.decisiontree = self.recursive(self.trainset, self.atts, 0)
    
    def root2leaf(self, node, inputs):
        if node.children is None:
            label = node.label
        else:
            keys = list(node.children.keys())
            #keys.remove('sample_weights')
            #if len(keys) < 20:
            #    print(inputs[node.att], keys)
            #print(keys)
            if inputs[node.att] not in keys:
                label = node.label
            else:
                label = self.root2leaf(node.children[inputs[node.att]], inputs)
        return label
    
    def predict(self, inputs):
        #print(inputs)
        #assert len(inputs) == 1
        label = self.root2leaf(self.decisiontree, inputs)
        return label 
        
    def accuracy(self, ypred, ytrue):
        n = len(ypred)
        assert n == len(ytrue)
        count = 0
        for i in range(n):
            if ypred[i] == ytrue[i]:
                count += 1
        return 1.0*count/n

class BaggingTrees(DecisionTree):
    def __init__(self, num_trees, trainset, label, frac, max_depth, min_exp, sample_weights=None):
        self.trees = dict()
        self.num_trees = num_trees
        self.label = label
        self.frac = frac
        self.trainset = trainset
        self.max_depth = max_depth
        self.min_exp = min_exp
        self.sample_weights = sample_weights
        if sample_weights:
            self.trainset['sample_weights'] = self.sample_weights
        self.buildtrees()
        
    def buildtrees(self):
        for i in range(self.num_trees):
            set4train = self.trainset.sample(frac=self.frac, random_state=np.random.randint(0, 100), replace=True)
            set4train.reset_index(drop=True, inplace=True)
            if self.sample_weights:
                sample_weights = list(set4train.loc[:, 'sample_weights'])
            else:
                sample_weights = None
            self.trees[i] = DecisionTree(set4train, self.label, self.max_depth, self.min_exp, False, sample_weights)
            self.trees[i].fit()
    
    def majority(self, series):
        lst = list(series)
        lst.sort()
        pre = lst[0]
        vals = [[pre, 1]]
        count = 1
        num = 1
        for i in range(1, len(lst)):
            if lst[i] != pre:
                count += 1
                pre = lst[i]
                vals.append([pre, 1])
            else:
                vals[-1][1] += 1
        assert count == len(vals)
        assert len(lst) == np.sum([ vals[i][1] for i in range(count)])
        votes = [vals[i][1] for i in range(count)]
        ind = np.argmax(votes)
        maj = vals[ind][0]
        return maj
            
    def predict(self, inputs):
        prediction = []
        for j in range(self.num_trees):
           prediction.append(self.trees[j].predict(inputs))
        pred = self.majority(prediction)
        return pred
        
    def accuracy(self, ypred, ytrue):
        n = len(ypred)
        assert n == len(ytrue)
        count = 0
        for i in range(n):
            if ypred[i] == ytrue[i]:
                count += 1
        return 1.0*count/n    
        
class RandomForest(DecisionTree):
    def __init__(self, num_trees, trainset, label, frac, max_depth, min_exp, sample_weights=None):
        self.trees = dict()
        self.num_trees = num_trees
        self.label = label
        self.frac = frac
        self.trainset = trainset
        self.max_depth = max_depth
        self.min_exp = min_exp
        self.sample_weights = sample_weights
        if sample_weights:
            self.trainset['sample_weights'] = self.sample_weights
        self.buildtrees()
        
    def buildtrees(self):
        for i in range(self.num_trees):
            set4train = self.trainset.sample(frac=self.frac, random_state=np.random.randint(0, 100), replace=True)
            set4train.reset_index(drop=True, inplace=True)
            if self.sample_weights:
                sample_weights = list(set4train.loc[:, 'sample_weights'])
            else:
                sample_weights = None
            self.trees[i] = DecisionTree(set4train, self.label, self.max_depth, self.min_exp, True, sample_weights)
            self.trees[i].fit()
    
    def majority(self, series):
        lst = list(series)
        lst.sort()
        pre = lst[0]
        vals = [[pre, 1]]
        count = 1
        num = 1
        for i in range(1, len(lst)):
            if lst[i] != pre:
                count += 1
                pre = lst[i]
                vals.append([pre, 1])
            else:
                vals[-1][1] += 1
        assert count == len(vals)
        assert len(lst) == np.sum([ vals[i][1] for i in range(count)])
        votes = [vals[i][1] for i in range(count)]
        ind = np.argmax(votes)
        maj = vals[ind][0]
        return maj
            
    def predict(self, inputs):
        prediction = []
        for j in range(self.num_trees):
           prediction.append(self.trees[j].predict(inputs))
        pred = self.majority(prediction)
        return pred
        
    def accuracy(self, ypred, ytrue):
        n = len(ypred)
        assert n == len(ytrue)
        count = 0
        for i in range(n):
            if ypred[i] == ytrue[i]:
                count += 1
        return 1.0*count/n

class AdaBoost(DecisionTree):
    def __init__(self, T, trainset, config):
        self.T = T
        self.config = config
        self.trainset = trainset
        self.models = dict()
        self.epsilon = []
        self.alpha = []
        self.sample_weights = [1.0/len(self.trainset)]*len(self.trainset)

    def createmodels(self, t, sample_weights):
        if self.config['model'] == 'DT':
            #for i in range(self.k):
            self.models[t] = DecisionTree(self.trainset, self.config['label'], self.config['max_depth'], self.config['min_exp'], False, sample_weights)
            self.models[t].fit()
        elif self.config['model'] == 'BT':
            #for i in range(self.k):
             self.models[t] = BaggingTrees(self.config['num_trees'], self.trainset, self.config['label'], self.config['frac'], self.config['max_depth'], self.config['min_exp'], sample_weights)
        elif self.config['model'] == 'RF':
            #for i in range(self.k):
             self.models[t] = RandomForest(self.config['num_trees'], self.trainset, self.config['label'], self.config['frac'], self.config['max_depth'], self.config['min_exp'], sample_weights)
             
    def weightederror(self, trainset, pred):
        epsilon = 0.0
        for i in range(len(pred)):
            #print(i, len(pred), pred[i])
            if trainset.iloc[i][self.config['label']] != pred[i]:
                epsilon += trainset.iloc[i]['sample_weights']
        return epsilon         
             
    def new_sample_weights(self, ytrue, ypred, t):
        D = []
        for i in range(len(ytrue)):
            D.append(self.sample_weights[i]*np.exp(-1.0*self.alpha[t]*ytrue[i]*ypred[i]))
        sumD = np.sum(D)
        print(sumD)
        D /= sumD
        self.sample_weights = D
             
    def fit(self):
        for t in range(self.T):
            self.createmodels(t, self.sample_weights)
            pred = []
            for i in range(len(self.trainset)):
                #print(t, self.models)
                pred.append(self.models[t].predict(self.trainset.iloc[i][:]))
            self.epsilon.append(self.weightederror(self.models[t].trainset, pred))
            self.alpha.append(1.0/2*np.log((1-self.epsilon[t])/self.epsilon[t]))
            self.new_sample_weights(list(self.trainset.loc[:, self.config['label']]), pred, t)
        
    def predict(self, inputs):
        pred = 0.0
        for t in range(self.T):
            #print(t)
            pred += self.alpha[t]*self.models[t].predict(inputs)
        pred = np.sign(pred)
        if pred == 0:
            pred = -1
        return pred    
        
class kFoldValidation:
    def __init__(self, k, trainset, config, t_frac=1):
        self.k = k
        self.trainset = trainset
        self.models = dict()
        self.config = config
        self.validationset = dict()
        self.trainset_k = dict()
        self.t_frac = t_frac
        self.createsets()
        self.createmodels()
    
    def partition(self, n, k):
        m = int(n/k)
        ind = dict()
        for i in range(k-1):
            ind[i] = []
            for j in range(m):
                ind[i].append(i*m+j)
        ind[k-1] = []
        j = (k-1)*m
        while j < n:
            ind[k-1].append(j)
            j += 1
        return ind
        
    def createsets(self):
        ind = self.partition(len(self.trainset), self.k)
        whole = list(range(len(self.trainset)))
        for i in range(self.k):
            self.validationset[i] = self.trainset.iloc[ind[i]][:]
            res = list(set(whole).difference(set(ind[i])))
            self.trainset_k[i] = self.trainset.iloc[res][:]
            self.trainset_k[i] = self.trainset_k[i].sample(frac=self.t_frac, random_state=32)
            self.trainset_k[i].reset_index(drop=True, inplace=True)
        self.fraction = len(self.trainset_k[0])
        #print(self.t_frac, self.trainset_k[0])
         
    def createmodels(self):
        if self.config['models'] == 'DT':
            for i in range(self.k):
                self.models[i] = DecisionTree(self.trainset_k[i], self.config['label'], self.config['max_depth'], self.config['min_exp'])
                self.models[i].fit()
        elif self.config['models'] == 'BT':
            for i in range(self.k):
                self.models[i] = BaggingTrees(self.config['num_trees'], self.trainset_k[i], self.config['label'], self.config['frac'], self.config['max_depth'], self.config['min_exp'])
        elif self.config['models'] == 'RF':
            for i in range(self.k):
                self.models[i] = RandomForest(self.config['num_trees'], self.trainset_k[i], self.config['label'], self.config['frac'], self.config['max_depth'], self.config['min_exp'])
        elif self.config['models'] == 'AB':
            for i in range(self.k):
                self.models[i] = AdaBoost(self.config['T'], self.trainset_k[i], self.config)
                self.models[i].fit()
                
    def crossvalidation(self):
        self.accu = []
        for i in range(self.k):
            pred = []
            for j in range(len(self.validationset[i])):
                pred.append(self.models[i].predict(self.validationset[i].iloc[j][:]))
            accu = self.models[i].accuracy(pred, list(self.validationset[i].loc[:, self.config['label']]))
            self.accu.append(accu)
        self.accu_avg = np.mean(self.accu)
        self.stderr = self.standarderror(self.accu)
    
    def standarderror(self, lst):
        mean = np.mean(lst)
        var = 0.0
        for i in range(len(lst)):
            var += np.square(lst[i]-mean)/len(lst)
        var = np.sqrt(var)
        return var/np.sqrt(len(lst))

def RMSD(ypred, ytrue):
    rmsd = 0.0
    for i in range(len(ypred)):
        rmsd += np.square(ypred[i]-ytrue[i])/len(ypred)
    rmsd = np.sqrt(rmsd)
    return rmsd       
 
def decisionTree(trainingSet, testSet):
    lb2bins = np.load('labels2bins.npy', allow_pickle = True)
    trainset = pd.read_csv(trainingSet)
    testset = pd.read_csv(testSet)
    label = 'SalePriceBin'
    max_depth = 8
    min_exp = 25
    #print(max_depth, min_exp)
    DT = DecisionTree(trainset, label, max_depth, min_exp)
    DT.fit()
    pred = []
    prices = []
    for i in range(len(trainset)):
        pred.append(DT.predict(trainset.iloc[i][:]))
        prices.append(lb2bins[pred[-1]])
        #print(pred[-1], testset.loc[i, label])
    accu_train = DT.accuracy(pred, list(trainset.loc[:, label]))
    rmsd_train = RMSD(prices, list(trainset.loc[:, 'SalePrice']))
    pred = []
    prices = []
    for i in range(len(testset)):
        pred.append(DT.predict(testset.iloc[i][:]))
        prices.append(lb2bins[pred[-1]])
        #print(pred[-1], testset.loc[i, label])
    accu_test = DT.accuracy(pred, list(testset.loc[:, label]))
    rmsd_test = RMSD(prices, list(testset.loc[:, 'SalePrice']))
    print('Training Accuracy DT:', accu_train, 'RMSD:', rmsd_train)
    print('Testing Accuracy DT:', accu_test, 'RMSD:', rmsd_test)
    
    testingset = pd.read_csv('test-binned.csv')
    pred = []
    prices = []
    for i in range(len(testingset)):
        pred.append(DT.predict(testingset.iloc[i][:]))
        prices.append(np.exp(lb2bins[pred[-1]]))
    #print(lb2bins)
    d = {'Id': list(testingset.loc[:, 'Id']), 'SalePrice': prices}
    prediction = pd.DataFrame(data=d)
    prediction.to_csv('pred_dt.csv', index=False)


def bagging(trainingSet, testSet):
    lb2bins = np.load('labels2bins.npy', allow_pickle = True)
    frac = 0.9
    num_trees = 30
    trees = dict()
    trainset = pd.read_csv(trainingSet)
    testset = pd.read_csv(testSet)
    label = 'SalePriceBin'
    max_depth = 8
    min_exp = 25
    BT = BaggingTrees(num_trees, trainset, label, frac, max_depth, min_exp)
    pred = []
    prices = []
    for i in range(len(trainset)):
        pred.append(BT.predict(trainset.iloc[i][:]))
        prices.append(lb2bins[pred[-1]])
    accu_train = BT.accuracy(pred, list(trainset.loc[:, label]))
    rmsd_train = RMSD(prices, list(trainset.loc[:, 'SalePrice']))
    pred = []
    prices = []
    for i in range(len(testset)):
        pred.append(BT.predict(testset.iloc[i][:]))
        prices.append(lb2bins[pred[-1]])
    accu_test = BT.accuracy(pred, list(testset.loc[:, label]))
    rmsd_test = RMSD(prices, list(testset.loc[:, 'SalePrice']))
    print('Training Accuracy BT:', accu_train, 'RMSD:', rmsd_train)
    print('Testing Accuracy BT:', accu_test, 'RMSD:', rmsd_test)
    
    testingset = pd.read_csv('test-binned.csv')
    pred = []
    prices = []
    for i in range(len(testingset)):
        pred.append(BT.predict(testingset.iloc[i][:]))
        prices.append(np.exp(lb2bins[pred[-1]]))
    d = {'Id': list(testingset.loc[:, 'Id']), 'SalePrice': prices}
    prediction = pd.DataFrame(data=d)
    prediction.to_csv('pred_bt.csv', index=False)
    
def randomForest(trainingSet, testSet):
    lb2bins = np.load('labels2bins.npy', allow_pickle = True)
    frac = 0.9
    num_trees = 50
    trees = dict()
    trainset = pd.read_csv(trainingSet)
    testset = pd.read_csv(testSet)
    label = 'SalePriceBin'
    max_depth = 8
    min_exp = 25
    RF = RandomForest(num_trees, trainset, label, frac, max_depth, min_exp)
    pred = []
    prices = []
    for i in range(len(trainset)):
        pred.append(RF.predict(trainset.iloc[i][:]))
        prices.append(lb2bins[pred[-1]])
    accu_train = RF.accuracy(pred, list(trainset.loc[:, label]))
    rmsd_train = RMSD(prices, list(trainset.loc[:, 'SalePrice']))
    pred = []
    prices = []
    for i in range(len(testset)):
        pred.append(RF.predict(testset.iloc[i][:]))
        prices.append(lb2bins[pred[-1]])
    accu_test = RF.accuracy(pred, list(testset.loc[:, label]))
    rmsd_test = RMSD(prices, list(testset.loc[:, 'SalePrice']))
    print('Training Accuracy RF:', accu_train, 'RMSD:', rmsd_train)
    print('Testing Accuracy RF:', accu_test, 'RMSD:', rmsd_test)
    
    testingset = pd.read_csv('test-binned.csv')
    pred = []
    prices = []
    for i in range(len(testingset)):
        pred.append(RF.predict(testingset.iloc[i][:]))
        prices.append(np.exp(lb2bins[pred[-1]]))
    d = {'Id': list(testingset.loc[:, 'Id']), 'SalePrice': prices}
    prediction = pd.DataFrame(data=d)
    prediction.to_csv('pred_rf.csv', index=False)

def zero2negone(sets, label):
    for i in list(sets.index):
        if sets.loc[i, label] == 0:
            sets.loc[i, label] = -1

def adaboost(trainingSet, testSet):
    lb2bins = np.load('labels2bins.npy', allow_pickle = True)
    config = dict()
    T = 20
    config['frac'] = 0.9
    config['num_trees'] = 30
    trees = dict()
    trainset = pd.read_csv(trainingSet)
    testset = pd.read_csv(testSet)
    config['label'] = 'SalePriceBin'
    config['max_depth'] = 8
    config['min_exp'] = 25
    config['model'] = 'DT'
    zero2negone(trainset, config['label'])
    zero2negone(testset, config['label'])
    AB = AdaBoost(T, trainset, config)
    AB.fit()
    pred = []
    prices = []
    for i in range(len(trainset)):
        pred.append(AB.predict(trainset.iloc[i][:]))
        prices.append(lb2bins[pred[-1]])
    accu_train = AB.models[0].accuracy(pred, list(trainset.loc[:, config['label']]))
    rmsd_train = RMSD(prices, list(trainset.loc[:, 'SalePrice']))
    pred = []
    prices = []
    for i in range(len(testset)):
        pred.append(AB.predict(testset.iloc[i][:]))
        prices.append(lb2bins[pred[-1]])
    accu_test = AB.models[0].accuracy(pred, list(testset.loc[:, config['label']]))
    rmsd_test = RMSD(prices, list(testset.loc[:, 'SalePrice']))
    print('Training Accuracy AdaBoost:', accu_train, 'RMSD:', rmsd_train)
    print('Testing Accuracy AdaBoost:', accu_test, 'RMSD:', rmsd_test)
    
    testingset = pd.read_csv('test-binned.csv')
    pred = []
    prices = []
    for i in range(len(testingset)):
        pred.append(AB.predict(testingset.iloc[i][:]))
        prices.append(np.exp(lb2bins[pred[-1]]))
    print(prices)
    d = {'Id': list(testingset.loc[:, 'Id']), 'SalePrice': prices}
    prediction = pd.DataFrame(data=d)
    prediction.to_csv('pred_ab.csv', index=False)
    
def xgboosts(trainingSet, testSet):
    lb2bins = np.load('labels2bins.npy', allow_pickle = True)
    trainset = pd.read_csv(trainingSet)
    testset = pd.read_csv(testSet)
    xgboost = XGBRegressor(learning_rate=0.02,
                       n_estimators=6000,
                       max_depth=5,
                       min_child_weight=0,
                       gamma=0.6,
                       subsample=0.7,
                       colsample_bytree=0.7,
                       objective='reg:linear',
                       nthread=-1,
                       scale_pos_weight=1,
                       seed=27,
                       reg_alpha=0.0006,
                       random_state=42)
    ytrain = trainset.loc[:, 'SalePrice']
    xtrain = trainset.drop(columns=['SalePrice', 'SalePriceBin', 'Id'])
    #xtrain = xtrain.drop(columns=nanvariables)
    xgboost.fit(xtrain, ytrain)
    ypred = xgboost.predict(xtrain)
    prices = ypred
    #print(ypred)
    #for i in range(len(ypred)):
        #pred.append(AB.predict(testingset.iloc[i][:]))
    #    prices.append(lb2bins[ypred[i]])
    #accu_train = accuracy(ypred, ytrain)
    accu_train = 'NA'
    rmsd_train = RMSD(prices, list(trainset.loc[:, 'SalePrice']))
    ytest = testset.loc[:, 'SalePriceBin']
    xtest = testset.drop(columns=['SalePrice', 'SalePriceBin', 'Id'])
    ypred = xgboost.predict(xtest)
    prices = ypred
    #for i in range(len(ypred)):
        #pred.append(AB.predict(testingset.iloc[i][:]))
    #    prices.append(lb2bins[ypred[i]])
    #accu_test = accuracy(ypred, ytest)
    accu_test = 'NA'
    rmsd_test = RMSD(prices, list(testset.loc[:, 'SalePrice']))
    print('Training Accuracy XGBoost:', accu_train, 'RMSD:', rmsd_train)
    print('Testing Accuracy XGBoost:', accu_test, 'RMSD:', rmsd_test)
    
    testingset = pd.read_csv('test-binned.csv')
    xtesting = testingset.drop(columns=['Id'])
    ypred = xgboost.predict(xtesting)
    prices = []
    for i in range(len(testingset)):
        #pred.append(xgboost.predict(testingset.iloc[i][:]))
        prices.append(np.exp(ypred[i]))
    #print(prices)
    d = {'Id': list(testingset.loc[:, 'Id']), 'SalePrice': prices}
    prediction = pd.DataFrame(data=d)
    prediction.to_csv('pred_xg.csv', index=False)
    
def accuracy(ypred, ytrue):
    n = len(ypred)
    count += 0.0
    for i in range(n):
        if ypred[i] == ytrue[i]:
            count += 1
    return 1.0*count/n

def main():
    trainingSet = sys.argv[1]
    testSet = sys.argv[2]
    modelIdx = sys.argv[3]
    if modelIdx == '1':
        decisionTree(trainingSet, testSet)
    elif modelIdx == '2':
        bagging(trainingSet, testSet)
    elif modelIdx == '3':
        randomForest(trainingSet, testSet)
    elif modelIdx == '4':
        adaboost(trainingSet, testSet)
    elif modelIdx == '5':
        xgboosts(trainingSet, testSet)

if __name__ == '__main__':
    main() 
