# global variable classes

categoricalvariables =  ['MSZoning', 'Street', 'Alley', 'LotShape', 'LandContour', 'Utilities', 'LotConfig', 'LandSlope', 'Neighborhood', 'Condition1', 'Condition2', 'BldgType', 'HouseStyle', 'RoofStyle', 'RoofMatl', 'Exterior1st', 'Exterior2nd', 'MasVnrType', 'ExterQual', 'ExterCond', 'Foundation', 'BsmtQual', 'BsmtCond', 'BsmtExposure', 'BsmtFinType1', 'BsmtFinType2', 'Heating', 'HeatingQC', 'CentralAir', 'Electrical', 'KitchenQual', 'Functional', 'FireplaceQu', 'GarageType', 'GarageFinish', 'GarageQual', 'GarageCond', 'PavedDrive', 'PoolQC', 'Fence', 'MiscFeature', 'SaleType', 'SaleCondition']

numericalvariables = ['MSSubClass', 'BedroomAbvGr', 'KitchenAbvGr', 'TotRmsAbvGrd',  'OverallQual', 'OverallCond']

continuousvariables = ['LotArea', 'BsmtFinSF1', 'BsmtFinSF2', 'BsmtUnfSF', 'TotalBsmtSF', '1stFlrSF', '2ndFlrSF', 'LowQualFinSF', 'GrLivArea', 'GarageArea', 'WoodDeckSF', 'OpenPorchSF', 'EnclosedPorch', '3SsnPorch', 'ScreenPorch', 'PoolArea', 'MiscVal', 'LotFrontage', 'MasVnrArea']


yearvariables = ['YearBuilt', 'YrSold', 'YearRemodAdd', 'GarageYrBlt']

othervariables = ['BsmtFullBath', 'BsmtHalfBath', 'FullBath', 'HalfBath', 'Fireplaces', 'GarageCars', 'MoSold']

nanvariables = ['LotFrontage', 'MasVnrArea', 'GarageYrBlt']


allvariables = categoricalvariables + numericalvariables + continuousvariables + othervariables + nanvariables + yearvariables

allvariables = list(set(allvariables))


categoricalvariables = list(set(categoricalvariables).difference(set(nanvariables)))
numericalvariables = list(set(numericalvariables).difference(set(nanvariables)))
continuousvariables = list(set(continuousvariables).difference(set(nanvariables)))
yearvariables = list(set(yearvariables).difference(nanvariables))
othervariables = list(set(othervariables).difference(nanvariables))
