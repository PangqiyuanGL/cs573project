import pandas as pd
import numpy as np
import sys
from sklearn.model_selection import train_test_split
from config import categoricalvariables, numericalvariables, continuousvariables, yearvariables, nanvariables, othervariables

class NBC:
    def __init__(self, trainset, atts, num_labels, labels2vals, classes_each_att):
        self.classes_each_att = classes_each_att
        self.num_labels = num_labels
        self.trainset = trainset
        self.labels2vals = labels2vals
        self.atts = atts
    
                 
    def _numofdiffvals(self, series):
        lst = list(series)
        n = len(lst)
        lst.sort()
        count = 1
        diffvals = [lst[0]]
        pre = lst[0]
        for i in range(1, n):
            if lst[i] != pre and lst[i] > -1:
                pre = lst[i]
                diffvals.append(lst[i])
                count += 1
        assert count == len(diffvals)
        return count, diffvals
        
    def prob_each_label(self):
        count = np.zeros(self.num_labels)
        for i in list(self.trainset.index):
            j = 0
            while j < self.num_labels:
                  if self.trainset.loc[i, 'SalePrice'] == j:
                      count[j] += 1
                      break
                  j += 1
        assert np.sum(count) == len(self.trainset)
        return [count[i]/np.sum(count) for i in range(self.num_labels)]
        
    def _prob_each_att(self, data):
        if len(data) == 0:
            flag = 0
        else:
            flag = 1
        prob = {}
        for att in self.atts:
            prob[att] = {}
            #print(att, list(self.classes_each_att[att].keys()))
            for i in list(self.classes_each_att[att].keys()):
                prob[att][i] = 0.0
            count = 0
            for i in list(data.index):
                if data.loc[i, att] < 0:
                    continue
                for j in list(self.classes_each_att[att].keys()):
                    if data.loc[i, att] == j:
                        prob[att][j] += 1
                        count += 1
                        break
            #assert count == len(data) or len(data) == 0
            #print(att, count, len(data))
            if count > 0:
                sums = 0.0
                #print(att, count)
                for i in list(self.classes_each_att[att].keys()):
                    prob[att][i] = prob[att][i]/count
                    sums += prob[att][i]
                assert abs(sums - 1.0) <= 1E-10
        return prob

    def prob_each_att_each_label(self):
        prob = {}
        data = self._splitdata_bylabels()
        for i in range(self.num_labels):
            prob[i] = self._prob_each_att(data[i])
        return prob
            
            
    def _splitdata_bylabels(self):
        dataset = {}
        for i in range(self.num_labels):
            dataset[i] = []
        for i in list(self.trainset.index):
            label = self.trainset.loc[i, 'SalePrice']
            dataset[label].append(i)
        results = {}
        for i in range(self.num_labels):
            results[i] = self.trainset.loc[dataset[i], :]
        return results
        
    def fit(self):
        self.prob_label = self.prob_each_label()
        self.prob_att_label = self.prob_each_att_each_label()
        
    def predict(self, x):
        # prediction will be the logorithm of SalePrice
        ind = list(x.index)
        n = len(ind)
        pred = np.zeros(n)
        for i in range(n):
            ii = ind[i]
            prob = np.zeros(self.num_labels)
            for j in range(self.num_labels):
                for att in self.atts:
                    if x.loc[ii, att] > -1:
                        #print(x.loc[ii, att], self.prob_att_label)
                        r = self.prob_att_label[j][att][x.loc[ii, att]]*self.prob_label[j]
                        if r < sys.float_info.min:
                            r = sys.float_info.min
                        prob[j] += np.log(r)
            pred[i] = self.labels2vals[np.argmax(prob)]
        return pred
        
    def evaluate(self, y_pred, y_true):
        assert len(y_pred) == len(y_true)
        n = len(y_pred)
        loss = 0.0
        for i in range(n):
            loss += (np.log(y_pred[i]) - np.log(y_true[i]))**2 / n
        loss = np.sqrt(loss)
        return loss
        
def main():
    b = 15
    num_labels = 10
    
    trainset = pd.read_csv('train-binned.csv')
    labels2vals = np.load('labels2bins.npy')
    binsassignment = np.load('binsassignment-mod.npy', allow_pickle = True).item()
    atts = list(trainset.keys())
    atts.remove('Id')
    atts.remove('SalePrice')
    atts = list(set(atts).difference(set(nanvariables)))
    ind = list(trainset.index)
    ind_train, ind_vad = train_test_split(ind, test_size=0.2, random_state = 33)
    X_train = trainset.loc[ind_train, :]
    X_vad = trainset.loc[ind_vad, :]
    model = NBC(X_train, atts, num_labels, labels2vals, binsassignment)
    model.fit()
    print('NBC trained!')
    pred_train = model.predict(X_train)
    accu_train = model.evaluate(pred_train, list(labels2vals[X_train.loc[:, 'SalePrice']]))
    print('Training loss:', accu_train)
    pred_vad = model.predict(X_vad)
    accu_vad = model.evaluate(pred_vad, list(labels2vals[X_vad.loc[:, 'SalePrice']]))
    print('Validation loss:', accu_vad)
    #trainset_original = pd.read_csv('train.csv')
    #trainset_original['SalePrice_pred'] = np.floor(np.exp(pred_train))
    #trainset_original.to_csv('train-pred.csv', index = False)
    
    testset = pd.read_csv('test-binned.csv')
    pred_test = model.predict(testset)
    results = {'Id': testset.loc[:, 'Id'], 'SalePrice': pred_test}
    results = pd.DataFrame(data=results)
    results.to_csv('test-pred-nbc.csv', index = False)
    print('New prediction has been made and saved!')
    
if __name__ == '__main__':
    main()
