import pandas as pd
import math
import numpy as np
from config import categoricalvariables, numericalvariables, continuousvariables, othervariables, nanvariables, yearvariables

class RandomForest:
   def __init__(self, trainset, labels, num_tree, atts, n):
       self.trainset = trainset
       self.num = len(trainset)
       self.labels = labels
       self.num_trees = num_trees
       self.atts = atts
       self.n = n
       
   def numofdiffvals(self, series):
       lst = list(series)
       lst.sort()
       pre = lst[0]
       vals = [[pre, 1]]
       count = 1
       num = 1
       for i in range(1, len(lst)):
           if lst[i] != pre:
               count += 1
               pre = lst[i]
               vals[-1][1] = num
               vals.append([pre, 1])
           else:
               num += 1
       assert count == len(vals)
       return count, vals
       
   def indices(self, series, vals):
       ind = dict()
       for i in range(len(vals)):
           ind[vals[i]] = []
       for i in list(series.index):
           for j in range(len(vals)):
               if series.loc[i, :] == vals[j][0]:
                   ind[vals[j]].append(i)
                   break
       for i in range(len(vals)):
           assert len(ind[i]) == vals[i][1]
       return ind
   
   def entropy(self, Set, att):
       count, vals = self.numofdiffvals(Set.loc[:, att])
       p = np.zeros(count)
       for i in range(count):
           p[i] = 1.0*vals[i][1]/count
       H = np.sum([-p[i]*np.log2(p[i]) for i in range(count)])
       return H

   def informationgain(self, Set, label, att):
       H = self.entropy(Set, label)
       count, vals = self.numofdiffvals(Set.loc[:, att])
       indices = self.indices(Set.loc[:, att], vals)
       entropies = []
       sums = 0.0
       for i in range(len(vals)):
           entropies.append(self.entropy(Set.loc[indices[i], :], label))
           sums += vals[i][1]/count * entropies[-1]
       infogain = H - sums
       return infogain
       
       
   def decisiontree(self, X, Y):
       
       
   def rf(self, ):
       trees = []
       for b in range(self.num_trees):
           sub_trainset_ind = np.random.randt(0, self.num, n)
           Xb = self.trainset.loc[sub_trainset_ind, :]
           Yb = self.labels.loc[sub_trainset_ind, :]
           trees.append(self.decisiontree(Xb, Yb))
