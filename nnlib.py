import tensorflow as tf
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from config import categoricalvariables, numericalvariables, continuousvariables, yearvariables, nanvariables, othervariables

class NNetwork:
   def __init__(self, trainset, labels, lr, batch_size, epochs, network_config, session, nn_input, nn_target):
       self.trainset = trainset
       self.labels = np.expand_dims(labels, axis = 1)
       self.batch_size = batch_size
       self.epochs = epochs
       self.lr = lr
       self.network_config = network_config
       self.session = session
       self.nn_input = nn_input
       self.nn_target = nn_target
       #self.initial = initial

   def init_weights(self, shape, name=None):
       return tf.Variable(tf.random_normal(shape, stddev = 0.01), name=name)
       
   def init_bias(self, shape, name=None):
       return tf.Variable(tf.zeros(shape, dtype='float'), name=name)
       
   def get_input_layer(self):
       net_input = tf.placeholder("float", [None, None], name='nn_input')
       net_target = tf.placeholder("float", [None, None], name='nn_target')
       return net_input, net_target
       
   def get_loss_layer(self, nn_out, target):
       mse = tf.reduce_mean(tf.square(nn_out-target))
       return mse
              
   def fcn(self, dim_input, nn_input, dim_output, nn_target, batch_size = 25, network_config = None):
       dim_hidden = network_config['dim_hidden'] + [dim_output]
       n_layers = len(dim_hidden)
       
       #nn_input, nn_target = self.get_input_layer()
       
       self.weights = dict()
       self.biases = dict()
       in_shape = dim_input
       for layer_step in range(0, n_layers):
           cur_weight = self.init_weights([in_shape, dim_hidden[layer_step]], name='w_' + str(layer_step))
           cur_bias = self.init_bias([dim_hidden[layer_step]], name='b_' + str(layer_step))
           #print('weights shape:', [in_shape, dim_hidden[layer_step]], 'bias shape', dim_hidden[layer_step])
           tf.add_to_collection(tf.GraphKeys.WEIGHTS, cur_weight)
           in_shape = dim_hidden[layer_step]
           self.weights[layer_step] = cur_weight
           self.biases[layer_step] = cur_bias
           
       regularizer = tf.contrib.layers.l2_regularizer(self.network_config['scale'])
       reg_term = tf.contrib.layers.apply_regularization(regularizer)
       
       cur_top = nn_input
       for layer_step in range(0, n_layers):
           if layer_step == 0:
               cur_top = tf.nn.relu(tf.matmul(cur_top, self.weights[layer_step]) + self.biases[layer_step])
           elif 0 < layer_step < n_layers-1:
               cur_top = tf.nn.relu(tf.matmul(cur_top, self.weights[layer_step]) + self.biases[layer_step])
           else:
               cur_top = tf.matmul(cur_top, self.weights[layer_step]) + self.biases[layer_step]
  
       loss = self.get_loss_layer(cur_top, nn_target)
       loss += reg_term
       return loss, cur_top
       
   #def prediction(self, inputs):
   #    cur_top = self.session.run(self.predict, feed_dict={nn_input: inputs})
   #    return cur_top.eval(session=self.session)
       
   def evaluate(self, y_pred, y_true):
       return np.sqrt(np.mean(np.square(y_pred-y_true)))
       
   def fit(self):
       n = len(self.trainset)
       m = len(self.trainset[0])
       #print('m = ', m)
       #nn_input, nn_target = self.get_input_layer()
       loss, predict = self.fcn(m, self.nn_input, 1, self.nn_target, self.batch_size, self.network_config)
       #print(predict)
       initial = tf.global_variables_initializer()
       train_step = tf.train.GradientDescentOptimizer(learning_rate=self.lr).minimize(loss)
       self.predict = predict
       #sess = self.session
       if self.session:
           self.session.run(initial)
           for i in range(self.epochs):
               j = 0
               while j < n-1:
                   start = j
                   end = min(start+self.batch_size, n-1)
                   X_batch = self.trainset[start:end,:]
                   Y_batch = self.labels[start:end, :]
                   self.session.run(train_step, feed_dict={self.nn_input: X_batch, self.nn_target: Y_batch})
                   #print(self.biases[2].eval(sess))
                   #print(self.session.run(self.session.graph.get_tensor_by_name('b_2')))
                   j += self.batch_size
               pred = self.session.run(self.predict, feed_dict={self.nn_input: self.trainset})
               loss1 = self.evaluate(pred, self.labels)
               print('Epoch:', i, loss1)
    
   
def main():
    X = pd.read_csv('train-binned.csv')
    Y = X.loc[:, 'SalePrice']
    X = X.drop(columns=['SalePrice', 'SalePriceBin'])
    X = np.array(X, dtype=np.float32)
    Y = np.array(Y, dtype=np.float32)
    X_train, X_vad, Y_train, Y_vad = train_test_split(X, Y, test_size=0.2, random_state = 33)
    lr = 0.0002
    batch_size = 25
    epochs = 300
    network_config = dict()
    network_config['dim_hidden'] = [30, 20, 20]
    network_config['scale'] = 1.0
    session = tf.Session()
    nn_input = tf.placeholder("float", [None, None], name='nn_input')
    nn_target = tf.placeholder("float", [None, None], name='nn_target')
    model = NNetwork(X_train, Y_train, lr, batch_size, epochs, network_config, session, nn_input, nn_target)
    model.fit()
    vald = session.run(model.predict, feed_dict={nn_input: X_vad})
    loss_vald = model.evaluate(vald, Y_vad)
    print('Neural network trained!')
    print('Loss on validation set:', loss_vald)
    testset = pd.read_csv('test-binned.csv')
    test = np.array(testset, dtype=np.float32)
    pred_test = session.run(model.predict, feed_dict={nn_input: test})
    pred_test = np.squeeze(pred_test)
    results = {'Id': list(testset.loc[:, 'Id']), 'SalePrice': np.exp(pred_test)}
    results = pd.DataFrame(data=results)
    results.to_csv('pred_nn.csv', index = False)
    print('New prediction has been made and saved!')
    
if __name__ == '__main__':
    main()
