'''
Preprocessing training data
Every value in each column in 'categoricalvariables' is transformed to an int alphabetically. For a column in 'continuousvariables' and 'yearvariables'(nanvariables is in the union of them), a range of all values is computed and then divided into b subranges equally. Each value is specified to an int according to which subrange it lies in. All other columns stay unchanged.  

Preprocessed training data will be saved as train-binned.csv
Split log(SalePrice) in train.csv into num_labels labels and save assignment info as labels2bins.npy
Set all NA to -1
Split columns with NA values 
'''

import pandas as pd
import math
import numpy as np
import datetime
import random
from config import categoricalvariables, numericalvariables, continuousvariables, othervariables, nanvariables, yearvariables

# Plots
import seaborn as sns
import matplotlib.pyplot as plt



# Stats
from scipy.stats import skew, norm
from scipy.special import boxcox1p
from scipy.stats import boxcox_normmax

# Misc
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import KFold, cross_val_score
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import LabelEncoder
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import scale
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import RobustScaler
from sklearn.decomposition import PCA

pd.set_option('display.max_columns', None)

# Ignore useless warnings
import warnings
warnings.filterwarnings(action="ignore")
pd.options.display.max_seq_items = 8000
pd.options.display.max_rows = 8000

import os
#print(os.listdir("../input/kernel-files"))


def tolowercase(data, attributes):
    n = len(data)
    for att in attributes:
        for i in range(n):
            if type(data.loc[i, att]) == float:
                if math.isnan(data.loc[i, att]):
                    data.loc[i, att] = 'na'
                else:
                    print(i, att, data.loc[i, att])
            else:
                data.loc[i, att] = data.loc[i, att].lower()
                
def numofdiffvals_str(series):
    lst = list(series)
    n = len(lst)
    lst.sort()
    count = 1
    diffvals = [lst[0]]
    pre = lst[0]
    #whattype = type(lst[0])
    for i in range(1, n):
        if lst[i] != pre and lst[i]:
            pre = lst[i]
            diffvals.append(lst[i])
            count += 1
    assert count == len(diffvals)
    return count, diffvals
    
def numofdiffvals_float(series):
    lst = list(series)
    n = len(lst)
    lst.sort()
    count = 1
    diffvals = [lst[0]]
    pre = lst[0]
    #whattype = type(lst[0])
    for i in range(1, n):
        if lst[i] != pre and not math.isnan(lst[i]):
            pre = lst[i]
            diffvals.append(lst[i])
            count += 1
    assert count == len(diffvals)
    return count, diffvals
    
def modbinsassignment(binsassignment):
    binsassignment_cate = dict()
    for att in categoricalvariables:
        binsassignment_cate[att] = dict()
        for key in list(binsassignment[att].keys()):
            binsassignment_cate[att][binsassignment[att][key]] = binsassignment[att][key]
        del binsassignment[att]
    binsassignment.update(binsassignment_cate)
    return binsassignment      


def tobins(data, b):
    n = len(data)
    binsassignment_cate = dict()
    for i in range(len(categoricalvariables)):
        binsassignment_cate[categoricalvariables[i]] = dict()
    for att in categoricalvariables:
        count, diffvals = numofdiffvals_str(data.loc[:, att])
        #print(att, count)
        binsassignment_cate[att] = {diffvals[i]: i for i in range(count)}
        for i in range(n):
            ind = diffvals.index(data.loc[i, att])
            data.loc[i, att] = ind
        data[att] = data[att].astype('int')
    
    binsassignment_cont = dict()
    for att in continuousvariables:
        rng = ranges(data, att)
        pts = np.linspace(rng[0], rng[1], b[att]+1)
        binsassignment_cont[att] = dict()
        for i in range(b[att]):
            binsassignment_cont[att][i] = i 
        for i in range(n):
            if not math.isnan(data.loc[i, att]):
                ind = assign(pts, data.loc[i, att])
                data.loc[i, att] = ind
            else:
                data.loc[i, att] = -1
        #if att not in nanvariables:
        data[att] = data[att].astype('int')
                
    binsassignment_year = dict()
    for att in yearvariables:
        rng = ranges(data, att)
        pts = np.linspace(rng[0], rng[1], b[att]+1)
        binsassignment_year[att] = dict()
        for i in range(b[att]):
            binsassignment_year[att][i] = i 
        for i in range(n):
            if not math.isnan(data.loc[i, att]):
                ind = assign(pts, data.loc[i, att])
                data.loc[i, att] = ind
            else:
                data.loc[i, att] = -1
        #if att not in nanvariables:
        data[att] = data[att].astype('int')
            
    binsassignment_nume = dict()
    for att in numericalvariables:
        data[att] = data[att].astype('int')
        count, diffvals = numofdiffvals_float(data.loc[:, att])
        binsassignment_nume[att] = dict()
        for i in diffvals:
            binsassignment_nume[att][i] = i 
            
    binsassignment_othe = dict()
    for att in othervariables:
        count, diffvals = numofdiffvals_float(data.loc[:, att])
        binsassignment_othe[att] = dict()
        for i in diffvals:
            binsassignment_othe[att][int(i)] = int(i)
        for i in range(n):
            if math.isnan(data.loc[i, att]):
                data.loc[i, att] = -1
                #print(att)
        data[att] = data[att].astype('int')
        
             
    binsassignment = dict()
    binsassignment.update(binsassignment_cate)
    binsassignment.update(binsassignment_cont)
    binsassignment.update(binsassignment_year)
    binsassignment.update(binsassignment_nume)
    binsassignment.update(binsassignment_othe)
    return binsassignment

def lb2bins(num_labels, data):
    n = len(data)
    for i in range(n):
        #data.loc[i, 'SalePrice'] = np.log(data.loc[i, 'SalePrice'])   
        ii = i
    rng = ranges(data, 'SalePrice')
    #print(rng)
    pts = np.linspace(rng[0], rng[1], num_labels+1)
    #print(rng, pts)
    indices = []
    for i in range(n):
        ind = assign(pts, data.iloc[i]['SalePrice'])
        #print(ind)
        indices.append(ind)
    data['SalePrice'] = indices
    data['SalePrice'] = data['SalePrice'].astype('int')
    #print(data['SalePrice'])
    labels2bins = np.zeros(num_labels)
    for i in range(num_labels):
        labels2bins[i] = (pts[i]+pts[i+1])/2
    return labels2bins
        
def assign(pts, val):
    if not (val >= pts[0] and val <= pts[-1]):
        print(val, pts)
    n = len(pts)
    for i in range(n-1):
        if val == pts[0]:
            return 0
        elif pts[i] < val and val <= pts[i+1]:
            return i    
    
def ranges(data, att):
    lst = list(data.loc[:, att])
    if (att in continuousvariables) and (att not in nanvariables):
        maxs = max(lst)
        mins = min(lst)
    elif (att in yearvariables) and (att not in nanvariables):
        maxs = max(lst)
        mins = min(lst)
    elif att in nanvariables:
        mins, maxs = findmaxmin_nan(data, att)
        #if att not in yearvariables:
        #    mins = 0.0
    else:
        mins, maxs = findmaxmin_nan(data, att)
        #mins = 0.0
    return [mins, maxs]
        
def findmaxmin_nan(data, att):
    lst = list(data.loc[:, att])
    n = len(lst)
    for i in range(n):
        if not math.isnan(lst[i]):
            mins = lst[i]
            maxs = lst[i]
            break
    for i in range(n):
        if math.isnan(lst[i]):
            continue
        else:
            if lst[i] < mins:
                mins = lst[i]
            elif lst[i] > maxs:
                maxs = lst[i]
    return mins, maxs
            
def replacenan(data, att):
    for i in range(len(data)):
        if data.iloc[i][att] is not None:
            val = data.iloc[i][att]
            break
    for i in range(len(data)):
        if data.iloc[i][att] is None:
            data.iloc[i][att] = val

def main():
    train = pd.read_csv('./train.csv')
    test = pd.read_csv('./test.csv')
    print(train.shape, test.shape)

    sns.set_style('white')
    sns.set_color_codes(palette='deep')
    f, ax = plt.subplots(figsize = (8,7))
    sns.distplot(train['SalePrice'], color='b')
    ax.xaxis.grid(False)
    ax.set(ylabel='Frequency')
    ax.set(xlabel='SalePrice')
    ax.set(title='SalePrice Distribution')
    sns.despine(trim=True, left=True)
    plt.savefig('salepricedist.png')
    #plt.show()
    
    numeric_dtypes = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']
    numeric = []
    for i in train.columns:
        if train[i].dtype in numeric_dtypes:
            if i in ['TotalSF', 'Total_Bathrooms','Total_porch_sf','haspool','hasgarage','hasbsmt','hasfireplace']:
                pass
            else:
                numeric.append(i)
    fig, axis = plt.subplots(ncols=3, nrows=12, figsize=(48, 120))
    #plt.subplots_adjust(right=3)
    #plt.subplots_adjust(top=3)
    sns.color_palette('husl', 10)

    for i, feature in enumerate(list(train[numeric]), 1):
        if(feature=='MiscVal'):
            break
        #print(i, feature)
        plt.subplot(len(list(numeric))/3, 3, i)
        sns.scatterplot(x=feature, y='SalePrice', hue='SalePrice', palette='Blues', data=train)
    
        plt.xlabel('{}'.format(feature), size=15, labelpad=12.5)
        plt.ylabel('SalePrice', size=15, labelpad=12.5)
    
        for j in range(2):
            plt.tick_params(axis='x', labelsize=12)
            plt.tick_params(axis='y', labelsize=12)
    
        plt.legend(loc='best', prop={'size':10})
    
    plt.savefig('vitualization.png')
    #plt.show()   
    
    corr = train.corr()
    plt.subplots(figsize=(15,12))
    sns_plot = sns.heatmap(corr, vmax=0.9, cmap = 'Blues', square=True)
    sns_plot.figure.savefig('correlation.png')

    data = pd.concat([train['SalePrice'], train['OverallQual']], axis=1)
    f, ax = plt.subplots(figsize=(8, 6))
    fig = sns.boxplot(x=train['OverallQual'], y="SalePrice", data=data)
    fig.axis(ymin=0, ymax=800000)

    data = pd.concat([train['SalePrice'], train['YearBuilt']], axis=1)
    f, ax = plt.subplots(figsize=(16, 8))
    fig = sns.boxplot(x=train['YearBuilt'], y="SalePrice", data=data)
    fig.axis(ymin=0, ymax=800000);
    plt.xticks(rotation=45)

    data = pd.concat([train['SalePrice'], train['TotalBsmtSF']], axis=1)
    data.plot.scatter(x='TotalBsmtSF', y='SalePrice', alpha=0.3, ylim=(0,800000))

    data = pd.concat([train['SalePrice'], train['LotArea']], axis=1)
    data.plot.scatter(x='LotArea', y='SalePrice', alpha=0.3, ylim=(0,800000))

    data = pd.concat([train['SalePrice'], train['GrLivArea']], axis=1)
    data.plot.scatter(x='GrLivArea', y='SalePrice', alpha=0.3, ylim=(0,800000))

    train["SalePrice"] = np.log(train["SalePrice"])
    #print(train['SalePrice'])
    
    sns.set_style("white")
    sns.set_color_codes(palette='deep')
    f, ax = plt.subplots(figsize=(8, 7))
    #Check the new distribution 
    sns.distplot(train['SalePrice'] , fit=norm, color="b");

    # Get the fitted parameters used by the function
    (mu, sigma) = norm.fit(train['SalePrice'])
    print( '\n mu = {:.2f} and sigma = {:.2f}\n'.format(mu, sigma))

    #Now plot the distribution
    plt.legend(['Normal dist. ($\mu=$ {:.2f} and $\sigma=$ {:.2f} )'.format(mu, sigma)],
            loc='best')
    ax.xaxis.grid(False)
    ax.set(ylabel="Frequency")
    ax.set(xlabel="SalePrice")
    ax.set(title="SalePrice distribution")
    sns.despine(trim=True, left=True)

    plt.savefig('salepricedist_log.png')
    #plt.show()

    train.drop(train[(train['OverallQual']<5) & (train['SalePrice']>200000)].index, inplace=True)
    train.drop(train[(train['GrLivArea']>4500) & (train['SalePrice']<300000)].index, inplace=True)
    train.drop(train[(train['LotFrontage']>300) & (train['SalePrice']<300000)].index, inplace=True)
    train.drop(train[(train['BsmtFinSF1']>5000) & (train['SalePrice']<300000)].index, inplace=True)
    train.drop(train[(train['TotalBsmtSF']>5500) & (train['SalePrice']<300000)].index, inplace=True)
    train.drop(train[(train['1stFlrSF']>4000) & (train['SalePrice']<300000)].index, inplace=True)
    train.reset_index(drop=True, inplace=True)


    b = {'LotArea': 40, 'BsmtFinSF1': 30, 'BsmtFinSF2': 10, 'BsmtUnfSF': 33, 'TotalBsmtSF': 35, '1stFlrSF': 31, '2ndFlrSF': 21, 'LowQualFinSF': 18, 'GrLivArea': 35, 'GarageArea': 25, 'WoodDeckSF': 13, 'OpenPorchSF': 9, 'EnclosedPorch': 6, '3SsnPorch': 15, 'ScreenPorch': 20, 'PoolArea': 10, 'MiscVal': 18, 'LotFrontage': 38, 'MasVnrArea': 27, 'YearBuilt': 20, 'YrSold': 5, 'YearRemodAdd': 18, 'GarageYrBlt': 35}
    for att in list(b.keys()):
        b[att] = int(b[att]/2)
    num_labels = 28
    data_train = train
    data_test = test
    #loglabels = np.log(data_train['SalePrice'])
    loglabels = data_train['SalePrice']
    #print(loglabels)
    labels2bins = lb2bins(num_labels, data_train)
    #print(labels2bins)
    
    nanvariablesbutneed = ['BsmtFinSF1', 'BsmtFinSF2', 'BsmtUnfSF', 'TotalBsmtSF', 'BsmtFullBath', 'BsmtHalfBath', 'GarageCars']
    
    
    keys_train = list(data_train.keys())
    keys_train.remove('SalePrice')
    keys_test = list(data_test.keys())
    #print(keys_train)
    #print(keys_test)
    assert keys_train == keys_test
    
    ind_train = list(data_train.index)
    data_full = pd.concat([data_train.drop(columns=['SalePrice']), data_test], ignore_index=True)
    tolowercase(data_full, categoricalvariables)
    
    for att in nanvariablesbutneed:
        replacenan(data_full, att)
        
    numeric_dtypes = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']
    numeric = []
    for i in data_full.columns:
        if data_full[i].dtype in numeric_dtypes:
            numeric.append(i)    
    
    sns.set_style('white')
    f, ax = plt.subplots(figsize=(8, 7))
    ax.set_xscale('log')
    ax = sns.boxplot(data=data_full[numeric], orient='h', palette='Set1')
    ax.xaxis.grid(False)
    ax.set(ylabel='Feature names')
    ax.set(xlabel='Numberic values')
    ax.set(title='Numberic Distribution of Features')
    sns.despine(trim=True, left=True)
    ax.figure.savefig('boxnumeric.png')
    
    skew_features = data_full[numeric].apply(lambda x: skew(x)).sort_values(ascending=False)
    high_skew = skew_features[skew_features > 0.5]
    skew_index = high_skew.index
    print("There are {} numerical features with Skew > 0.5 :".format(high_skew.shape[0]))
    skewness = pd.DataFrame({'Skew' :high_skew})
    print(skew_features.head(10))
    
    for i in skew_index:
        data_full[i] = boxcox1p(data_full[i], boxcox_normmax(data_full[i]+1))
    sns.set_style("white")
    f, ax = plt.subplots(figsize=(8, 7))
    ax.set_xscale("log")
    ax = sns.boxplot(data=data_full[skew_index] , orient="h", palette="Set1")
    ax.xaxis.grid(False)
    ax.set(ylabel="Feature names")
    ax.set(xlabel="Numeric values")
    ax.set(title="Numeric Distribution of Features")
    sns.despine(trim=True, left=True)
    ax.figure.savefig('boxplot_skew.png')
    
    '''
    data_full['BsmtFinType1_Unf'] = 1*(data_full['BsmtFinType1'] == 'Unf')
    data_full['HasWoodDeck'] = (data_full['WoodDeckSF'] == 0) * 1
    data_full['HasOpenPorch'] = (data_full['OpenPorchSF'] == 0) * 1
    data_full['HasEnclosedPorch'] = (data_full['EnclosedPorch'] == 0) * 1
    data_full['Has3SsnPorch'] = (data_full['3SsnPorch'] == 0) * 1
    data_full['HasScreenPorch'] = (data_full['ScreenPorch'] == 0) * 1
    data_full['YearsSinceRemodel'] = data_full['YrSold'].astype(int) -      data_full['YearRemodAdd'].astype(int)
    data_full['Total_Home_Quality'] = data_full['OverallQual'] + data_full['OverallCond']
    #data_full = data_full.drop(['Utilities', 'Street', 'PoolQC',], axis=1)
    data_full['TotalSF'] = data_full['TotalBsmtSF'] + data_full['1stFlrSF'] + data_full['2ndFlrSF']
    data_full['YrBltAndRemod'] = data_full['YearBuilt'] + data_full['YearRemodAdd']

    data_full['Total_sqr_footage'] = (data_full['BsmtFinSF1'] + data_full['BsmtFinSF2'] + data_full['1stFlrSF'] + data_full['2ndFlrSF'])
    data_full['Total_Bathrooms'] = (data_full['FullBath'] + (0.5 * data_full['HalfBath']) + data_full['BsmtFullBath'] + (0.5 *    data_full['BsmtHalfBath']))
    data_full['Total_porch_sf'] = (data_full['OpenPorchSF'] + data_full['3SsnPorch'] + data_full['EnclosedPorch'] + data_full['ScreenPorch'] + data_full['WoodDeckSF'])
    data_full['TotalBsmtSF'] = data_full['TotalBsmtSF'].apply(lambda x: np.exp(6) if x <= 0.0 else x)
    data_full['2ndFlrSF'] = data_full['2ndFlrSF'].apply(lambda x: np.exp(6.5) if x <= 0.0 else x)
    data_full['GarageArea'] = data_full['GarageArea'].apply(lambda x: np.exp(6) if x <= 0.0 else x)
    data_full['GarageCars'] = data_full['GarageCars'].apply(lambda x: 0 if x <= 0.0 else x)
    data_full['LotFrontage'] = data_full['LotFrontage'].apply(lambda x: np.exp(4.2) if x <= 0.0 else x)
    data_full['MasVnrArea'] = data_full['MasVnrArea'].apply(lambda x: np.exp(4) if x <= 0.0 else x)
    data_full['BsmtFinSF1'] = data_full['BsmtFinSF1'].apply(lambda x: np.exp(6.5) if x <= 0.0 else x)
    data_full['haspool'] = data_full['PoolArea'].apply(lambda x: 1 if x > 0 else 0)
    data_full['has2ndfloor'] = data_full['2ndFlrSF'].apply(lambda x: 1 if x > 0 else 0)
    data_full['hasgarage'] = data_full['GarageArea'].apply(lambda x: 1 if x > 0 else 0)
    data_full['hasbsmt'] = data_full['TotalBsmtSF'].apply(lambda x: 1 if x > 0 else 0)
    data_full['hasfireplace'] = data_full['Fireplaces'].apply(lambda x: 1 if x > 0 else 0)
    '''
    
    
    '''
    for att in list(b.keys()):
        count, vals = numofdiffvals_float(data_full.loc[:, att])
        print(att, count)
    '''
    ind_full = list(data_full.index)
    binsassignment = tobins(data_full, b)
    np.save('binsassignment.npy', binsassignment)
    binsassignment_mod = modbinsassignment(binsassignment)
    
    data_full = data_full.drop(columns=nanvariables)
    ind_test = list(set(ind_full).difference(set(ind_train)))
    d_train = data_full.loc[ind_train, :]
    d_test = data_full.loc[ind_test, :]
    salepricebin = list(data_train['SalePrice'])
    n = len(list(d_train.columns))
    d_train.insert(n, 'SalePriceBin', salepricebin)
    d_train.insert(n+1, 'SalePrice', loglabels)
    #d_train['SalePriceBin'] = salepricebin
    #d_train['SalePrice'] = loglabels
    #print(data_train['SalePrice'])
    
    validation = d_train.sample(frac=0.1)
    ind_vad = list(validation.index)
    d_train.drop(ind_vad)
    validation.to_csv('validation.csv', index = False)
    print('Validation data have been binned and saved.')
    d_train.to_csv('train-binned.csv', index = False)
    print('Training data have been binned and saved.')
    d_test.to_csv('test-binned.csv', index = False)
    print('Testing data have been binned and saved.')
    #d_train['SalePrice'] = loglabels
    #d_train.to_csv('train-binned-loglabels.csv', index = False)
    print('Training data have been binned and saved(with loglabels).')
    np.save('labels2bins.npy', labels2bins)
    np.save('binsassignment-mod.npy', binsassignment_mod)
    
    
if __name__ == '__main__':
    main()
